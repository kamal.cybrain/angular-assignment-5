import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationRoutingModule } from './authentication/authentication-routing.module';
import { HomeRoutingModule } from './home/home-routing.module';


const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', loadChildren: () => import('./home/home.module').then(m => m.HomeModule) , data: { title: 'Dashboard' }}, 
  { path: 'auth', loadChildren: () => import('./authentication/authentication.module').then(m => m.AuthenticationModule),  data: { title: 'Auth Module' } }];

// const routes: Routes = [
//   { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
//   { path: 'dashboard', loadChildren: () => import('./home/home.module').then(m => m.HomeModule), data: { title: 'Dashboard' } },
//   { path: 'auth', loadChildren: () => import('./authentication/authentication.module').then(m => m.AuthenticationModule), data: { title: 'Auth Module' } },
//   { path: '**', redirectTo: '/dashboard', pathMatch: 'full' }
// ];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
